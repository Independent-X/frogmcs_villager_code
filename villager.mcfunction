/summon minecraft:villager ~ ~1 ~ {CustomName:"\"人口贩子\"",
ActiveEffects:[{Id:10,Amplifier:254,Duration:9999999}],
ArmorItems:[{},{},{id:"minecraft:turtle_helmet",Count:1b,tag:{Enchantments:[{id:"minecraft:thorns",lvl:999999},{id:"minecraft:protection",lvl:999999}]}},{}],
ArmorDropChances:[-1f,-1f,-1f,-1f],
VillagerData:{profession:fletcher,level:5,type:swamp},NoAI:1,
Rotation:[90f,0f],
Offers:{Recipes:[

{maxUses:99999999,buy:{id:"bookshelf",Count:1b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"家具建材商\""},
EntityTag:{NoAI:1b,HandItems:[{id:"bookshelf",Count:1b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type: "savanna", profession: "mason", lvl: 5},Offers:{Recipes:[
{buy:{id:emerald,Count:2},sell:{id:glowstone,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:9},sell:{id:bookshelf,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:lantern,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:glass,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:2},sell:{id:campfire,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:2},sell:{id:painting,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:slime_ball,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:36},sell:{id:bell,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:6},sell:{id:blue_ice,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:3},sell:{id:packed_ice,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:red_sand,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:sand,Count:8},maxUses:9999999},
{buy:{id:emerald,Count:2},sell:{id:sea_pickle,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:brick,Count:12},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"brick",Count:1b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"瓦匠\""},
EntityTag:{NoAI:1b,HandItems:[{id:"brick",Count:1b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type: "savanna", profession: "mason", lvl: 5},Offers:{Recipes:[
{buy:{id:emerald,Count:1},sell:{id:white_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:orange_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:magenta_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:light_glazed_blue_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:yellow_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:lime_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:pink_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:gray_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:light_glazed_gray_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:cyan_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:purple_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:blue_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:brown_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:green_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:red_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:black_glazed_terracotta,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:white_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:orange_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:magenta_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:light_blue_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:yellow_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:lime_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:pink_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:gray_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:light_gray_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:cyan_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:purple_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:blue_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:brown_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:green_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:red_terracotta,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:black_terracotta,Count:6},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"iron_ingot",Count:1b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"矿物商\""},
EntityTag:{NoAI:1b,HandItems:[{id:"iron_ingot",Count:1b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type: "savanna", profession: "mason", lvl: 5},Offers:{Recipes:[
{buy:{id:coal,Count:10},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:iron_ingot,Count:4},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:gold_ingot,Count:3},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:diamond,Count:1},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:coal,Count:5},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:redstone,Count:2},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:iron_ingot,Count:2},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:gold_ingot,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:2},sell:{id:diamond,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},buyB:{id:gravel,Count:10},sell:{id:flint,Count:10},maxUses:9999999},
{buy:{id:flint,Count:25},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:flint,Count:1},sell:{id:gravel,Count:1},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"white_dye",Count:1b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"文具店店员\""},
EntityTag:{NoAI:1b,HandItems:[{id:"white_dye",Count:1b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"librarian",profession:"minecraft:fletcher",lvl:5},Offers:{Recipes:[
{buy:{id:emerald,Count:5},sell:{id:clock,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:4},sell:{id:compass,Count:1},maxUses:9999999},
{buy:{id:paper,Count:24},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:ink_sac,Count:5},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:writable_book,Count:1},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:book,Count:4},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:7},sell:{id:map,Count:1},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"lapis_lazuli",Count:1b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"魔法书店店员\""},
EntityTag:{NoAI:1b,HandItems:[{id:"lapis_lazuli",Count:1b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"savanna",profession:"cleric",lvl:5},Offers:{Recipes:[
{buy:{id:emerald,Count:1},sell:{id:lapis_lazuli,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:14},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:mending,lvl:1}]}},maxUses:9999999},
{buy:{id:emerald,Count:7},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:silk_touch,lvl:1}]}},maxUses:9999999},
{buy:{id:emerald,Count:12},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:fortune,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:16},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:looting,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:15},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:efficiency,lvl:4}]}},maxUses:9999999},
{buy:{id:emerald,Count:32},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:efficiency,lvl:5}]}},maxUses:9999999},
{buy:{id:emerald,Count:62},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:sharpness,lvl:5}]}},maxUses:9999999},
{buy:{id:emerald,Count:27},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:blast_protection,lvl:4}]}},maxUses:9999999},
{buy:{id:emerald,Count:24},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:feather_falling,lvl:4}]}},maxUses:9999999},
{buy:{id:emerald,Count:21},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:fire_protection,lvl:4}]}},maxUses:9999999},
{buy:{id:emerald,Count:39},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:riptide,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:59},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:bane_of_arthropods,lvl:5}]}},maxUses:9999999},
{buy:{id:emerald,Count:9},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:channeling,lvl:1}]}},maxUses:9999999},
{buy:{id:emerald,Count:7},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:multishot,lvl:1}]}},maxUses:9999999},
{buy:{id:emerald,Count:7},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:flame,lvl:1}]}},maxUses:9999999},
{buy:{id:emerald,Count:8},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:aqua_affinity,lvl:1}]}},maxUses:9999999},
{buy:{id:emerald,Count:29},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:quick_charge,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:14},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:vanishing_curse,lvl:1}]}},maxUses:9999999},
{buy:{id:emerald,Count:12},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:binding_curse,lvl:1}]}},maxUses:9999999},
{buy:{id:emerald,Count:51},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:impaling,lvl:4}]}},maxUses:9999999},
{buy:{id:emerald,Count:18},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:frost_walker,lvl:2}]}},maxUses:9999999},
{buy:{id:emerald,Count:11},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:infinity,lvl:1}]}},maxUses:9999999},
{buy:{id:emerald,Count:42},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:depth_strider,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:28},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:smite,lvl:5}]}},maxUses:9999999},
{buy:{id:emerald,Count:18},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:frost_walker,lvl:1}]}},maxUses:9999999},
{buy:{id:emerald,Count:32},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:thorns,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:18},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:fire_aspect,lvl:2}]}},maxUses:9999999},
{buy:{id:emerald,Count:12},buyB:{id:book,Count:1},sell:{id:enchanted_book,Count:1,tag:{StoredEnchantments:[{id:unbreaking,lvl:3}]}},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"beacon",Count:1b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"冠位盔甲武器工具匠\""},
EntityTag:{NoAI:1b,HandItems:[{id:"beacon",Count:1b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"savanna",profession:"weaponsmith",lvl:5},Offers:{Recipes:[
{buy:{id:emerald,Count:1},sell:{id:chainmail_helmet,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:4},sell:{id:chainmail_chestplate,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:3},sell:{id:chainmail_leggings,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:chainmail_boots,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:3},sell:{id:iron_pickaxe,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:3},sell:{id:iron_axe,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:2},sell:{id:iron_sword,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:iron_hoe,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:iron_helmet,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:4},sell:{id:iron_chestplate,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:3},sell:{id:iron_leggings,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:iron_boots,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:22},sell:{id:diamond_pickaxe,Count:1,tag:{Enchantments:[{id:unbreaking,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:22},sell:{id:diamond_axe,Count:1,tag:{Enchantments:[{id:unbreaking,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:20},sell:{id:diamond_sword,Count:1,tag:{Enchantments:[{id:unbreaking,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:4},sell:{id:diamond_hoe,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:20},sell:{id:diamond_helmet,Count:1,tag:{Enchantments:[{id:unbreaking,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:23},sell:{id:diamond_chestplate,Count:1,tag:{Enchantments:[{id:unbreaking,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:22},sell:{id:diamond_leggings,Count:1,tag:{Enchantments:[{id:unbreaking,lvl:3}]}},maxUses:9999999},
{buy:{id:emerald,Count:19},sell:{id:diamond_boots,Count:1,tag:{Enchantments:[{id:unbreaking,lvl:3}]}},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"poisonous_potato",Count:3},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"食材收购商\""},
EntityTag:{NoAI:1b,HandItems:[{id:"poisonous_potato",Count:3},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"plains",profession:"shepherd",lvl:5},Offers:{Recipes:[
{buy:{id:potato,Count:26},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:carrot,Count:22},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:pumpkin,Count:6},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:melon,Count:4},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:wheat,Count:20},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:beetroot,Count:15},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:chicken,Count:14},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:porkchop,Count:7},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:rabbit,Count:4},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:mutton,Count:7},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:beef,Count:10},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:dried_kelp_block,Count:10},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:sweet_berries,Count:10},sell:{id:emerald,Count:1},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"poppy",Count:3},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"花店店员\""},
EntityTag:{NoAI:1b,HandItems:[{id:"poppy",Count:3},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"desert",profession:"farmer",lvl:5},Offers:{Recipes:[
{buy:{id:emerald,Count:1},sell:{id:dandelion,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:poppy,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:blue_orchid,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:allium,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:azure_bluet,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:red_tulip,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:orange_tulip,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:white_tulip,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:pink_tulip,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:oxeye_daisy,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:cornflower,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:lily_of_the_valley,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:wither_rose,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:sunflower,Count:2},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:lilac,Count:2},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:rose_bush,Count:2},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:peony,Count:2},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:brown_mushroom,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:red_mushroom,Count:4},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"poisonous_potato",Count:3},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"食品批发商\""},
EntityTag:{NoAI:1b,HandItems:[{id:"poisonous_potato",Count:3},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"savanna",profession:"farmer",lvl:5},Offers:{Recipes:[
{buy:{id:emerald,Count:1},sell:{id:apple,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:3},sell:{id:cookie,Count:18},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:cake,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:3},sell:{id:golden_carrot,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:4},sell:{id:glistering_melon_slice,Count:3},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:pumpkin_pie,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:bread,Count:6},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:cooked_chicken,Count:8},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:cooked_porkchop,Count:5},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:rabbit_stew,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:suspicious_stew,Count:1,tag:{Effects:[{EffectId:23,EffectDuration:200}]}},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:suspicious_stew,Count:1,tag:{Effects:[{EffectId:1,EffectDuration:200}]}},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:suspicious_stew,Count:1,tag:{Effects:[{EffectId:8,EffectDuration:200}]}},maxUses:9999999},
{buy:{id:gold_block,Count:8},buyB:{id:apple,Count:1},sell:{id:enchanted_golden_apple,Count:1},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"pufferfish_bucket",Count:1b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"渔者\""},
EntityTag:{NoAI:1b,HandItems:[{id:"pufferfish_bucket",Count:1b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"savanna",profession:"fisherman",lvl:5},Offers:{Recipes:[
{buy:{id:cod,Count:15},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:salmon,Count:13},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:pufferfish,Count:4},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:tropical_fish,Count:6},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:stick,Count:32},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:tripwire_hook,Count:8},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:string,Count:13},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:ink_sac,Count:5},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:bone,Count:4},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:3},sell:{id:cod_bucket,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:3},sell:{id:salmon_bucket,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:3},sell:{id:pufferfish_bucket,Count:1},maxUses:9999999},
{buy:{id:spruce_boat,Count:1},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:birch_boat,Count:1},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:oak_boat,Count:1},sell:{id:emerald,Count:1},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"arrow",Count:3b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"冒险者工会商店\""},
EntityTag:{NoAI:1b,HandItems:[{id:"arrow",Count:3b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"savanna",profession:"shepherd",lvl:5},Offers:{Recipes:[
{buy:{id:emerald,Count:1},sell:{id:arrow,Count:16},maxUses:9999999},
{buy:{id:emerald,Count:2},buyB:{id:arrow,Count:5},sell:{id:tipped_arrow,Count:5,tag:{Potion:strong_regeneration}},maxUses:9999999},
{buy:{id:emerald,Count:2},buyB:{id:arrow,Count:5},sell:{id:tipped_arrow,Count:5,tag:{Potion:strong_instant_damage}},maxUses:9999999},
{buy:{id:string,Count:13},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:leather,Count:6},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:feather,Count:23},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:white_wool,Count:18},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:rotten_flesh,Count:32},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:bone,Count:4},sell:{id:emerald,Count:1},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"white_banner",Count:1b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"染坊-旗帜染色工\""},
EntityTag:{NoAI:1b,HandItems:[{id:"white_banner",Count:1b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"plains",profession:"butcher",lvl:5},Offers:{Recipes:[
{buy:{id:emerald,Count:1},sell:{id:white_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:orange_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:magenta_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:light_blue_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:yellow_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:lime_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:pink_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:gray_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:light_gray_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:cyan_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:purple_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:blue_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:brown_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:green_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:red_banner,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:black_banner,Count:1},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"white_dye",Count:1b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"染坊-染料工\""},
EntityTag:{NoAI:1b,HandItems:[{id:"white_dye",Count:1b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"plains",profession:"butcher",lvl:5},Offers:{Recipes:[
{buy:{id:red_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:green_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:purple_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:cyan_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:light_gray_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:gray_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:pink_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:lime_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:yellow_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:light_blue_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:magenta_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:orange_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:black_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:brown_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:blue_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:white_dye,Count:12},sell:{id:emerald,Count:1},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:red_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:green_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:purple_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:cyan_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:light_gray_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:gray_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:pink_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:lime_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:yellow_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:light_blue_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:magenta_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:orange_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:black_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:brown_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:blue_dye,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:white_dye,Count:4},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"white_wool",Count:1b},sell:{id:"minecraft:villager_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"染坊-羊毛染色工\""},
EntityTag:{NoAI:1b,HandItems:[{id:"white_wool",Count:1b},{}],HandDropChances:[1f,0.0f],
VillagerData:{type:"plains",profession:"butcher",lvl:5},Offers:{Recipes:[
{buy:{id:emerald,Count:1},sell:{id:white_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:orange_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:magenta_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:light_blue_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:yellow_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:lime_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:pink_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:gray_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:light_gray_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:cyan_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:purple_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:blue_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:brown_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:green_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:red_wool,Count:4},maxUses:9999999},
{buy:{id:emerald,Count:1},sell:{id:black_wool,Count:4},maxUses:9999999}
]},id:"minecraft:villager"}}}},

{maxUses:99999999,buy:{id:"grass_block",Count:64},sell:{id:"minecraft:wandering_trader_spawn_egg",Count:1b,tag:{Damage:1,display:{Name:"\"体验用流浪商人\""},
EntityTag:{NoAI:0,HandItems:[{id:"grass_block",Count:64},{}],HandDropChances:[1f,0.0f],
Offers:{Recipes:[
{buy:{id:stick,Count:32},sell:{id:emerald,Count:1},maxUses:9999999}
]},
DespawnDelay:99999999999,
WanderTarget: {X:302, Y:73, Z:118},
id:"wandering_trader"}}}}
]}}
