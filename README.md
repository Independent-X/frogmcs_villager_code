# FrogMCS_Villager_Code

Creation command codes of villagers.
For any modification and additional command, please submit PR and notify admins in chat channel.

---
command save:

1. get a op sword:
```
/give @p minecraft:diamond_sword{display:{Name:"[{\"text\":\"杀村民专用\",\"bold\":false,\"italic\":false,\"underlined\":false,\"strikethrough\":false,\"obfuscated\":false}]"},Enchantments:[{id:"minecraft:sharpness",lvl:999}]} 1
```

2. get a real villager's NBT:
```
/data get entity @e[limit=1, sort=nearest, type=minecraft:villager] {}
```
